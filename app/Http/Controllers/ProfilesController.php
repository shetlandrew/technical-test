<?php

namespace App\Http\Controllers;

use App\Profile;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {            
        // Validate fields
        request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:profiles',
            'photo'  => 'mimes:png,jpg|max:2048',
        ]);
        
        // Initialise profile
        $profile = new Profile();
        $profile->name = $request->input('name');
        $profile->email = $request->input('email');
            
        // Save original and resize photos, if an image was posted
        if ($request->hasFile('photo')) {
            try {
                // Store original photo
                $profile->file_original = $this->storeOriginalPhoto($request);

                // Make and store resized photo
                $profile->file_resized = $this->storeResizedPhoto($request);
            } catch (\Exception $e) {
                // Couldn't save images, so return error JSON
                $errors = [
                    'general' => ['Unable to save image.']
                ];

                return $this->getErrorResponse($errors);
            }
        }

        // Save profile to database
        try {
            $profile->save(); 

            $json = [
                'success' => true,
                'resized' => $profile->file_resized ?? ''
            ];
                
            // Only return original filename for feature testing
            if (app()->environment() === 'testing' && !empty($profile->file_original)) {
                $json['original'] = $profile->file_original ?? '';
            }
            
            return response()->json($json);
        } catch (\Exception $e) {
            // Couldn't save profile, so return error JSON
            $errors = [
                'general' => ['Unable to save profile.']
            ];

            return $this->getErrorResponse($errors);
        }
    }

    /**
     * Store original photo to storage (non-public)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function storeOriginalPhoto(Request $request): string
    {
        $original = $request->photo;
        $fileOriginal = ((string) Str::uuid());
        $fileOriginal .= ($original->extension() !== '' ? ".{$original->extension()}" : '');
        $original->storeAs('profiles', $fileOriginal);

        return $fileOriginal;
    }

    /**
     * Store resized photo to storage (public)
     * Image will be resized to no more than 500px wide and 500px tall maintaining aspect ratio
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */    
    protected function storeResizedPhoto(Request $request): string
    {
        $original = $request->photo;

        $width = 500;
        $height = 500;
        $resized = Image::make($original);
        $resized->height() > $resized->width() ? $width = null : $height = null;
        $resized = $resized->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 75);

        // Save resized image
        $fileResized = ((string) Str::uuid()) . '.jpg';
        Storage::put(
            "public/profiles/{$fileResized}",
            $resized->getEncoded()
        );

        return $fileResized;
    }

    /**
     * Create a response with a status code of 422 returning a generic message
     * and errors in its JSON
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */        
    protected function getErrorResponse(array $errors): JsonResponse
    { 
        return response()->json([
            'message' => "The given data was invalid.",
            'errors' => $errors
        ], 422);
    }
}

