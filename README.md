# Technical Test Implementation By Andrew Irvine

## Setup

### Run the following commands in the terminal
- `git clone git@bitbucket.org:shetlandrew/technical-test.git`
- `cd technical-test`
- `composer install`
- `yarn`
- `cp .env.example .env`

### Edit the .env file, making sure to change the following variables to match your environment
- `DB_HOST`
- `DB_PORT`
- `DB_DATABASE`
- `DB_USERNAME`
- `DB_PASSWORD`

### Generate application key
- `php artisan key:generate`

### Run database migrations
- `php artisan migrate`

### Serve app locally (http://127.0.0.1:8000)
- `php artisan serve`

## Run back-end tests (in the /tests folder)
- `./vendor/bin/phpunit`

## Run front-end tests (in the /resources/js/tests folder)
- `yarn jest`
