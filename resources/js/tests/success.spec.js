import { shallowMount } from '@vue/test-utils'

import Success from '../components/Success.vue'

test('it should mount', () => {
    const wrapper = shallowMount(Success)

    expect(wrapper).toMatchSnapshot()
})

describe('computed', () => {
    test('profileImagePath', () => {
        const { vm } = shallowMount(Success, {
            propsData: {
                profileImage: 'profile.jpg'
            }
        })

        expect(vm.profileImagePath).toBe('/storage/profiles/profile.jpg')
    })
})