import { shallowMount, mount } from '@vue/test-utils'

import Form from '../components/Form.vue'

window.axios = require('axios');

jest.mock('axios')
jest.useFakeTimers()

test('it should mount', () => {
    const wrapper = shallowMount(Form)

    expect(wrapper).toMatchSnapshot()
})

describe('methods', () => {
    test('submitForm', async () => {
        const wrapper = shallowMount(Form)

        const data = {
            "success": true,
            "resized": ""
        }

        axios.post.mockImplementation(() => Promise.resolve({ status: 200, data: data }));

        wrapper.vm.name = 'Test'
        wrapper.vm.email = 'test@example.com'

        await wrapper.vm.submitForm({
            preventDefault: () => { }
        })

        jest.advanceTimersByTime(100)

        await wrapper.vm.$nextTick()
        expect(wrapper.emitted().submitForm).toBeTruthy()        
    })    

    test('validateForm', () => {
        const wrapper = shallowMount(Form)

        wrapper.vm.name = ''
        wrapper.vm.email = 'test@example.com'

        const errors = {
            "name": ["The name field is required."]
        }

        expect(wrapper.vm.validateForm()).toStrictEqual(errors)
    })
})

describe('input', () => {
    test('required fields empty', async () => {
        const wrapper = shallowMount(Form)
        const button = wrapper.find('button')

        const errors = {
            "name": ["The name field is required."],
            "email": ["The email field is required."]
        }

        await button.trigger('click')

        expect(wrapper.vm.errors).toStrictEqual(errors)
    })

    test('invalid email', async () => {
        const wrapper = shallowMount(Form)
        const button = wrapper.find('button')

        const response = {
            "data": {
                "message": "The given data was invalid.",
                "errors": {
                    "email": ["The email must be a valid email address."]
                } 
            }
        }

        const errors = {
            "email": ["The email must be a valid email address."]
        }

        axios.post.mockImplementation(() => Promise.reject({ status: 422, response: response }));

        wrapper.vm.name = 'Test'
        wrapper.vm.email = 'test-example.com'
        await button.trigger('click')

        expect(wrapper.vm.errors).toStrictEqual(errors)
    })    

    test('valid', async () => {
        const wrapper = shallowMount(Form)
        const button = wrapper.find('button')

        const data = {
            "success": true,
            "resized": ""
        }

        axios.post.mockImplementation(() => Promise.resolve({ status: 200, data: data }));

        wrapper.vm.name = 'Test'
        wrapper.vm.email = 'test@example.com'
        await button.trigger('click')

        jest.advanceTimersByTime(100)

        await wrapper.vm.$nextTick()
        expect(wrapper.emitted().submitForm).toBeTruthy()
    })    
})