import { shallowMount } from '@vue/test-utils'

import App from '../App.vue'
import Success from '../components/Success.vue'

test('it should mount', () => {
    const wrapper = shallowMount(App)

    expect(wrapper).toMatchSnapshot()
})

describe('methods', () => {
    test('submitForm', () => {
        const { vm } = shallowMount(App)

        vm.submitForm('Test', 'profile.jpg')

        expect(vm.name).toBe('Test')
        expect(vm.profileImage).toBe('profile.jpg')

        expect(shallowMount(vm.currentView).is(Success)).toBe(true)
    })
})