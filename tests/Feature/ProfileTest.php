<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

use Intervention\Image\Facades\Image;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var array
     */
    protected $headers = [
        'Accept' => 'application/json',
        'content-type' => 'multipart/form-data',
        'X-Requested-With' => 'XMLHttpRequest'
    ];

    /**
     * The create page response has a status code of 200
     *
     * @return void
     */
    public function test_it_can_show_create_page()
    {
        $response = $this->get('/');

        // Returns status code of 200
        $response->assertStatus(200);
    }

    /**
     * Can store a profile (name and email) without a photo
     *
     * @return void
     */
    public function test_it_can_store_profile_without_photo()
    {
        $postData = [
            'name' => 'Test',
            'email' => 'test@example.com'
        ];

        $response = $this->post('/', $postData, $this->headers);

        // Returns status code of 200
        $response->assertStatus(200);

        // Returns success JSON
        $response->assertJson([
            "success" => true,
            "resized" => ''
        ]);


        $dbData = $postData + [
            'file_original' => null,
            'file_resized' => null
        ];

        // Profile stored in database (with null images)
        $this->assertDatabaseHas('profiles', $dbData);
    }       

    /**
     * Responds with an error if required fields are empty
     *
     * @return void
     */
    public function test_it_can_validate_required_fields()
    {
        $response = $this->post('/', [], $this->headers);

        // Returns status code of 422      
        $response->assertStatus(422);

        // Returns error JSON for all empty required fields
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "name" => ["The name field is required."],
                "email" => ["The email field is required."]
            ]
        ]);
    }

    /**
     * Responds with an error if email address is invalid
     *
     * @return void
     */
    public function test_it_can_validate_email_field()
    {
        $postData = [
            'name' => 'Test',
            'email' => 'test-example.com'
        ];

        $response = $this->post('/', $postData, $this->headers);

        // Returns status code of 422      
        $response->assertStatus(422);

        // Returns error JSON for invalid email address
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "email" => ["The email must be a valid email address."]
            ]
        ]);

        $postData['email'] = 'test@example.com';
        $response = $this->post('/', $postData, $this->headers);

        $postData['email'] = 'test@example.com';
        $response = $this->post('/', $postData, $this->headers);

        // Returns status code of 422      
        $response->assertStatus(422);

        // Returns error JSON for taken email address
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "email" => ["The email has already been taken."]
            ]
        ]);
    }     

    /**
     * Can upload a photo and store original and resized images
     *
     * @return void
     */
    public function test_it_can_upload_a_photo()
    {
        Storage::fake('local');

        $postData = [
            'name' => 'Test',
            'email' => 'test@example.com',
            'photo' => UploadedFile::fake()->image('profile.jpg', 800, 900)
        ];

        $response = $this->post('/', $postData, $this->headers);

        // Returns status code of 200        
        $response->assertStatus(200);

        // Returns success JSON        
        $response->assertJson([
            "success" => true
        ]);

        $responseData = $response->decodeResponseJson();

        // Original and resized images stored
        Storage::disk('local')->assertExists("profiles/{$responseData['original']}");
        Storage::disk('local')->assertExists("public/profiles/{$responseData['resized']}");

        // Resized images no larger than 500 pixels in width and height
        $resized = Image::make(Storage::disk('local')->get("public/profiles/{$responseData['resized']}"));
        $this->assertLessThanOrEqual(500, $resized->width());
        $this->assertLessThanOrEqual(500, $resized->height());

        $dbData = $postData;
        unset($dbData['photo']);
        $dbData += [
            'file_original' => $responseData['original'],
            'file_resized' => $responseData['resized']
        ];

        // Profile stored in database (with correct file paths)
        $this->assertDatabaseHas('profiles', $dbData);
    }

    /**
     * Responds with an error if photo is invalid
     *
     * @return void
     */
    public function test_it_can_validate_photo_field()
    {
        $postData = [
            'name' => 'Test',
            'email' => 'test@example.com',
            'photo' => UploadedFile::fake()->create('profile.jpg', 3000)
        ];

        $response = $this->post('/', $postData, $this->headers);

        // Returns status code of 422        
        $response->assertStatus(422);

        // Returns error JSON for a file that's too big
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "photo" => ["The photo may not be greater than 2048 kilobytes."]
            ]
        ]);

        $postData['photo'] = UploadedFile::fake()->create('document.pdf', 1024);

        $response = $this->post('/', $postData, $this->headers);

        // Returns status code of 422        
        $response->assertStatus(422);

        // Returns error JSON for a file of an incorrect type
        $response->assertJson([
            "message" => "The given data was invalid.",
            "errors" => [
                "photo" => ["The photo must be a file of type: png, jpg."]
            ]
        ]);        
    }       
}
